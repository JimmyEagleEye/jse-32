package ru.korkmasov.tsc.api.service;

import org.jetbrains.annotations.NotNull;
import ru.korkmasov.tsc.api.other.ISaltSettings;

public interface IPropertyService extends ISaltSettings {

    @NotNull String getApplicationVersion();

    int getBackupInterval();

    int getScannerInterval();

}
