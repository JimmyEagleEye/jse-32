package ru.korkmasov.tsc.api.service;

import ru.korkmasov.tsc.api.service.IPropertyService;

import org.jetbrains.annotations.NotNull;

public interface ServiceLocator {

    @NotNull ITaskService getTaskService();

    @NotNull IProjectService getProjectService();

    @NotNull IProjectTaskService getProjectTaskService();

    @NotNull ICommandService getCommandService();

    @NotNull IAuthService getAuthService();

    @NotNull IUserService getUserService();

    @NotNull IPropertyService getPropertyService();

}
