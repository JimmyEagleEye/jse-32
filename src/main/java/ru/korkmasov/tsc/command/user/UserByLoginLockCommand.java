package ru.korkmasov.tsc.command.user;

import ru.korkmasov.tsc.enumerated.Role;

import static ru.korkmasov.tsc.util.TerminalUtil.nextLine;

public final class UserByLoginLockCommand extends AbstractUserCommand {
    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Lock user by login";
    }

    @Override
    public void execute() {
        System.out.println("[LOCK USER]");
        System.out.println("ENTER LOGIN:");
        serviceLocator.getUserService().lockUserByLogin(nextLine());
    }
    @Override
    public String name() {
        return "user-lock-by-login";
    }

    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }
}
